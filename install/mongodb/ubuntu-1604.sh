#!/bin/bash

echo Import the public key used by the package management system.
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4

echo Create a list file for MongoDB.
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-4.0.list

echo Reload local package database.
apt-get update

echo Install the MongoDB packages.
apt-get install -y mongodb-org

echo Enable MongoDB
systemctl enable mongod

echo Start MongoDB
systemctl start mongod