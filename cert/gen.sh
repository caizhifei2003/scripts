#!/bin/bash

cert_name="idsrv"
cert_len=2048
cert_days=3650

echo "name: ${cert_name}"
echo "length: ${cert_len}"
echo "expire days: ${cert_days}"

openssl genrsa -out "${cert_name}.key" $cert_len
openssl req -new -key "${cert_name}.key" -out "${cert_name}.csr"
openssl x509 -req -days ${cert_days} -in "${cert_name}.csr" -signkey "${cert_name}.key" -out "${cert_name}.crt"
openssl pkcs12 -export -in "${cert_name}.crt" -inkey "${cert_name}.key" -out "${cert_name}.pfx"
